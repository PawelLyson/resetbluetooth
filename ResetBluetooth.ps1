﻿if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

function Reset-Device
{
    param
    (
        [string]$NameDeviceExist = "*",
        [string]$NameDeviceToReset
    )
    if (Get-WmiObject Win32_PNPEntity |Where-Object {$_.Name -like $DeviceName})
    {
        Start-Process -WindowStyle Hidden -Wait -FilePath "C:\Users\Komputer\PowerShell\ResetBluetooth\devcon.exe" -ArgumentList "disable $NameDeviceToReset";
        Start-Process -WindowStyle Hidden -Wait -FilePath "C:\Users\Komputer\PowerShell\ResetBluetooth\devcon.exe" -ArgumentList "enable $NameDeviceToReset";
    }
}

Reset-Device -NameDeviceToReset "USB\VID_105*" -NameDeviceExist "HP Bluetooth Mouse Z5000";
